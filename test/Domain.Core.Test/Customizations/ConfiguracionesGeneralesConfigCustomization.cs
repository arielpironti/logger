﻿using AutoFixture;
using Logger.Domain.Core.DTO.Config;
using Microsoft.Extensions.Options;
using Moq;

namespace Domain.Core.Test.Customizations
{
    public class ConfiguracionesGeneralesConfigCustomization : ICustomization
    {
        public void Customize(IFixture fixture)
        {
            fixture.Customize<ConfiguracionesGeneralesConfig>(
                x => x.With(c => c.FileLogName, "FileLog-{date}.txt")
                      .With(c => c.FileLogPath, "C:/Logs/")
            );

            var config = new Mock<IOptionsSnapshot<ConfiguracionesGeneralesConfig>>();
            config.SetupGet(mock => mock.Value).Returns(fixture.Create<ConfiguracionesGeneralesConfig>());
            fixture.Register(() => config.Object);
        }
    }
}