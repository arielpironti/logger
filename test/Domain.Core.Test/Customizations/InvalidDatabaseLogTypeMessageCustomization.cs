﻿using AutoFixture;
using Logger.Domain.Core.DTO.Request;

namespace Domain.Core.Test.Customizations
{
    public class InvalidDatabaseLogTypeMessageCustomization : ICustomization
    {
        public void Customize(IFixture fixture)
        {
            fixture.Customize<DatabaseLogMessage>(
                x => x.With(c => c.Description, "InvalidType")
            );
        }
    }
}