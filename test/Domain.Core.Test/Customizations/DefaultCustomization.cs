﻿using AutoFixture;
using AutoFixture.AutoMoq;

namespace Domain.Core.Test.Customizations
{
    public class DefaultCustomization : CompositeCustomization
    {
        public DefaultCustomization()
            : base(new AutoMoqCustomization())
        { }
    }
}