﻿using AutoFixture;
using Logger.Domain.Core.DTO.Request;
using Moq;

namespace Domain.Core.Test.Customizations
{
    public class LogMessageCustomization : ICustomization
    {
        public void Customize(IFixture fixture)
        {
            fixture.Customize<DatabaseLogMessage>(
                x => x.With(c => c.Description, It.IsAny<string>())
            );

            fixture.Customize<FileLogMessage>(
                x => x.With(c => c.Description, It.IsAny<string>())
            );

            fixture.Customize<LogMessage>(
               x => x.With(c => c.Description, It.IsAny<string>())
           );
        }
    }
}