﻿using AutoFixture.Xunit2;
using Domain.Core.Test.Attributes;
using Logger.Domain.Core.DTO.Request;
using Logger.Domain.Core.ExternalServices;
using Logger.Domain.Core.Services;
using Moq;
using System.Threading.Tasks;
using Xunit;

namespace Domain.Core.Test.Services
{
    public class ConsoleLoggerServiceTest
    {
        [Theory(DisplayName = "1.1 Verifica que se llame al servicio para logear en consola.")]
        [DefaultData()]
        public async Task LogToConsole_ShouldWord(
            [Frozen]Mock<IConsoleExternalService> mockConsoleExternalService,
            ConsoleLoggerService sut)
        {
            //Arrange
            mockConsoleExternalService.Setup(s => s.AddLogLine(It.IsAny<TypeMessage>(), It.IsAny<ConsoleLogMessage>()))
                                      .Returns(Task.CompletedTask);

            // Act
            await sut.AddLogMessageAsync(It.IsAny<TypeMessage>(), It.IsAny<ConsoleLogMessage>());


            // Assert
            mockConsoleExternalService.Verify(mock => mock.AddLogLine(It.IsAny<TypeMessage>(), It.IsAny<ConsoleLogMessage>()), Times.Exactly(1));
        }
    }
}