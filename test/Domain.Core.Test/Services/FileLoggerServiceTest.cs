﻿using AutoFixture;
using AutoFixture.Xunit2;
using Domain.Core.Services;
using Domain.Core.Test.Attributes;
using Domain.Core.Test.Customizations;
using Logger.Domain.Core.DTO.Request;
using Logger.Domain.Core.Repositories;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Domain.Core.Test.Services
{
    public class FileLoggerServiceTest
    {
        [Theory(DisplayName = "1.1 Verifica que se llame al repository para logear en file.")]
        [DefaultData(typeof(LogMessageCustomization),
                     typeof(ConfiguracionesGeneralesConfigCustomization))]
        public async Task LogToFile_ShouldWord(
            IFixture fixture,
            [Frozen]Mock<IFileRepository> _mockRepository,
            FileLoggerService sut)
        {
            //Arrange
            _mockRepository.Setup(s => s.AddContent(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()))
                                      .Returns(Task.CompletedTask);

            _mockRepository.Setup(s => s.AddContent(It.IsAny<string>()))
                                      .Returns(Task.CompletedTask);


            // Act
            await sut.AddLogMessageAsync(It.IsAny<TypeMessage>(), fixture.Create<FileLogMessage>());


            // Assert
            _mockRepository.Verify(mock => mock.AddContent(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()), Times.Exactly(1));
        }
    }
}
