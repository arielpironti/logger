﻿using AutoFixture;
using AutoFixture.Xunit2;
using Domain.Core.Test.Attributes;
using Domain.Core.Test.Customizations;
using Logger.Domain.Core.Data;
using Logger.Domain.Core.DTO.Request;
using Logger.Domain.Core.Repositories;
using Logger.Domain.Core.Services;
using Moq;
using System.Threading.Tasks;
using Xunit;

namespace Domain.Core.Test.Services
{
    public class DatabaseLoggerServiceTest
    {
        [Theory(DisplayName = "1.1 Verifica que se llame al repository para logear en base de datos.")]
        [DefaultData(typeof(LogMessageCustomization))]
        public async Task LogToDatabase_ShouldWord(
            IFixture fixture,
            [Frozen]Mock<ILogTypeRepository> mockLogTypeRepository,
            [Frozen]Mock<IUnitOfWork> mockUow,
            DatabaseLoggerService sut)
        {
            //Arrange
            mockLogTypeRepository.Setup(s => s.GetByName(It.IsAny<string>()))
                   .ReturnsAsync(fixture.Create<Logger.Domain.Entities.LogType>());

            mockUow.Setup(s => s.LogRepository.AddAsync(It.IsAny<Logger.Domain.Entities.Log>()))
                   .Returns(Task.CompletedTask);

            // Act
            await sut.AddLogMessageAsync(It.IsAny<TypeMessage>(), fixture.Create<DatabaseLogMessage>());

            // Assert
            mockLogTypeRepository.Verify(mock => mock.GetByName(It.IsAny<string>()), Times.Exactly(1));
            mockUow.Verify(mock => mock.LogRepository.AddAsync(It.IsAny<Logger.Domain.Entities.Log>()), Times.Exactly(1));
            mockUow.Verify(mock => mock.SaveAsync(), Times.Exactly(1));
        }
    }
}