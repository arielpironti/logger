﻿using AutoFixture;
using AutoFixture.Xunit2;
using Domain.Core.Test.Attributes;
using Domain.Core.Test.Customizations;
using Logger.Domain.Core.Data;
using Logger.Domain.Core.DTO.Request;
using Logger.Domain.Core.ExternalServices;
using Logger.Domain.Core.Repositories;
using Logger.Domain.Core.Services;
using Moq;
using System.Threading.Tasks;
using Xunit;

namespace Domain.Core.Test.Services
{
    public class LoggerAllServiceTest
    {
        [Theory(DisplayName = "1.1 Verifica que se llame a todos lo medios de logeo.")]
        [DefaultData(typeof(LogMessageCustomization),
                     typeof(ConfiguracionesGeneralesConfigCustomization))]
        public async Task LogAll_ShouldWord(
            IFixture fixture,
            [Frozen]Mock<IUnitOfWork> mockUow,
            [Frozen]Mock<ILogTypeRepository> mockLogTypeRepository,
            [Frozen]Mock<IConsoleExternalService> mockConsoleExternalService,
            [Frozen]Mock<IFileRepository> _mockRepository,
            LoggerAllService sut)
        {
            //Arrange
            _mockRepository.Setup(s => s.AddContent(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()))
                                      .Returns(Task.CompletedTask);

            _mockRepository.Setup(s => s.AddContent(It.IsAny<string>()))
                                      .Returns(Task.CompletedTask);

            mockLogTypeRepository.Setup(s => s.GetByName(It.IsAny<string>()))
                   .ReturnsAsync(fixture.Create<Logger.Domain.Entities.LogType>());

            mockUow.Setup(s => s.LogRepository.AddAsync(It.IsAny<Logger.Domain.Entities.Log>()))
                   .Returns(Task.CompletedTask);

            mockConsoleExternalService.Setup(s => s.AddLogLine(It.IsAny<TypeMessage>(), It.IsAny<ConsoleLogMessage>()))
                                      .Returns(Task.CompletedTask);

            // Act
            await sut.AddLogMessageAsync(It.IsAny<TypeMessage>(), fixture.Create<LogMessage>());

            // Assert
            mockConsoleExternalService.Verify(mock => mock.AddLogLine(It.IsAny<TypeMessage>(), It.IsAny<ConsoleLogMessage>()), Times.Exactly(1));

            _mockRepository.Verify(mock => mock.AddContent(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()), Times.Exactly(1));

            mockLogTypeRepository.Verify(mock => mock.GetByName(It.IsAny<string>()), Times.Exactly(1));
            mockUow.Verify(mock => mock.LogRepository.AddAsync(It.IsAny<Logger.Domain.Entities.Log>()), Times.Exactly(1));
            mockUow.Verify(mock => mock.SaveAsync(), Times.Exactly(1));
        }
    }
}