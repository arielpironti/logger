﻿using System;

namespace Logger.Infrastructure.Cache.Abstractions.Exceptions
{
    internal class DeserializingObjectException : Exception
    {
        public DeserializingObjectException(Exception ex)
            : base("Error deserializing object", ex)
        { }
    }
}