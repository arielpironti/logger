﻿using Logger.Domain.Core.Exceptions;

namespace Logger.Infrastructure.Cache.Abstractions.Exceptions
{
    public class NotFoundInCacheException : DomainException
    {
        public NotFoundInCacheException(string key)
            : base($"Key '{key}' is not an existing key")
        { }
    }
}