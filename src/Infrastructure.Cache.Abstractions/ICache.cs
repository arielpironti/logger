﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Logger.Infrastructure.Cache.Abstractions
{
    public interface ICache
    {
        Task SetAsync(string key, object obj, long? expirationInSeconds = null);

        Task<T> GetAsync<T>(string key);

        Task<T> GetOrDefaultAsync<T>(string key);

        Task<bool> IsInCacheAsync(string key);

        Task RemoveAsync(params string[] keys);

        Task RemoveAllAsync(string pattern);

        Task<List<string>> GetAllKeysAsync(string pattern = null);
    }
}