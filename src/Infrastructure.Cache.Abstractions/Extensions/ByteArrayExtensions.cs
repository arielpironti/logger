﻿using Logger.Infrastructure.Cache.Abstractions.Exceptions;
using Logger.Infrastructure.Cache.Abstractions.Helpers;
using Newtonsoft.Json;
using System.Text;

namespace System
{
    public static class ByteArrayExtensions
    {
        public static byte[] ToByteArray(this object obj)
        {
            if (obj == null)
                return null;

            if (obj is string s)
                return Encoding.UTF8.GetBytes(s);

            return Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(obj, new PrivateJsonSerializerSettings()));
        }

        public static T ToObject<T>(this byte[] bytes)
        {
            try
            {
                var str = Encoding.UTF8.GetString(bytes, 0, bytes.Length);
                if (typeof(T) == typeof(string))
                    return (T)(object)str;

                return JsonConvert.DeserializeObject<T>(str, new PrivateJsonSerializerSettings());
            }
            catch (Exception ex)
            {
                throw new DeserializingObjectException(ex);
            }
        }
    }
}