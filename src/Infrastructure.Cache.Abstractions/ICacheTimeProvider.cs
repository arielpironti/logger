using System;

namespace Logger.Infrastructure.Cache.Abstractions
{
    public interface ICacheTimeProvider
    {
        DateTimeOffset GetAbsoluteDateTime(long relativeTimeInSeconds);
    }
}