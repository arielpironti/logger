﻿using Newtonsoft.Json;

namespace Logger.Infrastructure.Cache.Abstractions.Helpers
{
    public class PrivateJsonSerializerSettings : JsonSerializerSettings
    {
        public PrivateJsonSerializerSettings()
        {
            ConstructorHandling = ConstructorHandling.AllowNonPublicDefaultConstructor;
            ContractResolver = new PrivateSetterContractResolver();
        }
    }
}