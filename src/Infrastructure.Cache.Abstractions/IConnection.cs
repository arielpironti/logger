using System;
using System.Threading.Tasks;

namespace Logger.Infrastructure.Cache.Abstractions
{
    public interface IConnection<TDatabase> : IDisposable
    {
        bool IsConnected { get; }

        Task ConnectAsync();

        Task<bool> TryConnectAsync();

        Task<TDatabase> GetDatabaseAsync();

        Task<TDatabase> GetDatabaseOrDefaultAsync();
    }
}