using System;

namespace Logger.Infrastructure.Cache.Abstractions
{
    public class CacheTimeProvider : ICacheTimeProvider
    {
        public virtual DateTimeOffset GetAbsoluteDateTime(long relativeTimeInSeconds) => DateTimeOffset.UtcNow.AddSeconds(relativeTimeInSeconds);
    }
}