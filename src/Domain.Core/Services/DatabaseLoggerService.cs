﻿using Logger.Domain.Core.Data;
using Logger.Domain.Core.DTO.Request;
using Logger.Domain.Core.Repositories;
using System;
using System.Threading.Tasks;

namespace Logger.Domain.Core.Services
{
    public class DatabaseLoggerService : LoggerService<DatabaseLogMessage>, ILoggerService<DatabaseLogMessage>
    {
        private readonly IUnitOfWork _uow;
        private readonly ILogTypeRepository _logTypeRepository;

        public DatabaseLoggerService(IUnitOfWork uow, ILogTypeRepository logTypeRepository)
            : base()
        {
            _uow = uow ?? throw new ArgumentNullException(nameof(uow));
            _logTypeRepository = logTypeRepository ?? throw new ArgumentNullException(nameof(logTypeRepository));
        }

        public async Task AddLogMessageAsync(TypeMessage typeMessage, DatabaseLogMessage message)
        {
            await AddMessageAsync(typeMessage, message);
        }

        protected override async Task AddMessageAsync(TypeMessage typeMessage, DatabaseLogMessage message)
        {
            var logType = await _logTypeRepository.GetByName(typeMessage.ToString());

            await _uow.LogRepository.AddAsync(new Entities.Log(message.Description, logType.Id));
            await _uow.SaveAsync();
        }
    }
}