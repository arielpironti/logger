﻿using Logger.Domain.Core.DTO.Config;
using Logger.Domain.Core.DTO.Request;
using Logger.Domain.Core.Repositories;
using Logger.Domain.Core.Services;
using Microsoft.Extensions.Options;
using System;
using System.Threading.Tasks;

namespace Domain.Core.Services
{
    public class FileLoggerService : LoggerService<FileLogMessage>, ILoggerService<FileLogMessage>
    {
        private readonly IFileRepository _repository;
        private readonly ConfiguracionesGeneralesConfig _configuracionesGenerales;

        public FileLoggerService(IFileRepository repository,
                                 IOptionsSnapshot<ConfiguracionesGeneralesConfig> configuracionesGenerales)
            : base()
        {
            _repository = repository ?? throw new ArgumentNullException(nameof(_repository));
            _configuracionesGenerales = configuracionesGenerales.Value ?? throw new ArgumentNullException(nameof(configuracionesGenerales));
        }

        public async Task AddLogMessageAsync(TypeMessage typeMessage, FileLogMessage message)
        {
            await AddMessageAsync(typeMessage, message);
        }

        protected override async Task AddMessageAsync(TypeMessage typeMessage, FileLogMessage message)
        {
            //Esta metodo de repository usa logeo de Nlog , para logear en archivo
            await _repository.AddContent($"{typeMessage.ToString().ToUpper()}|{message.Description}");

            await _repository.AddContent(_configuracionesGenerales.FileLogPath, _configuracionesGenerales.FileLogName.Replace("{date}", DateTime.Now.ToString("dd-MM-yyyy")), $"{typeMessage.ToString().ToUpper()}|{message.Description}");
        }
    }
}