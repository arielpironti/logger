﻿using Logger.Domain.Core.Data;
using Logger.Domain.Core.DTO.Config;
using Logger.Domain.Core.DTO.Request;
using Logger.Domain.Core.ExternalServices;
using Logger.Domain.Core.Repositories;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Threading.Tasks;

namespace Logger.Domain.Core.Services
{
    public class LoggerAllService : LoggerService<LogMessage>, ILoggerService<LogMessage>
    {
        private readonly IUnitOfWork _uow;
        private readonly IFileRepository _repository;
        private readonly IConsoleExternalService _service;
        private readonly ILogTypeRepository _logTypeRepository;
        protected readonly ILogger<LoggerAllService> _logger;
        private readonly ConfiguracionesGeneralesConfig _configuracionesGenerales;

        public LoggerAllService(IFileRepository repository,
                                ILogTypeRepository logTypeRepository,
                                IConsoleExternalService service,
                                IOptionsSnapshot<ConfiguracionesGeneralesConfig> configuracionesGenerales,
                                ILogger<LoggerAllService> logger,
                                IUnitOfWork uow)
            : base()
        {
            _repository = repository ?? throw new ArgumentNullException(nameof(_repository));
            _configuracionesGenerales = configuracionesGenerales.Value ?? throw new ArgumentNullException(nameof(configuracionesGenerales));
            _uow = uow ?? throw new ArgumentNullException(nameof(uow));
            _service = service ?? throw new ArgumentNullException(nameof(service));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _logTypeRepository = logTypeRepository ?? throw new ArgumentNullException(nameof(logTypeRepository));
        }

        public async Task AddLogMessageAsync(TypeMessage typeMessage, LogMessage message)
        {
            await AddMessageAsync(typeMessage, message);
        }

        protected override async Task AddMessageAsync(TypeMessage typeMessage, LogMessage message)
        {
            await AddLogMessageToFile(typeMessage, message);
            await AddLogMessageToDatabase(typeMessage, message);
            await AddLogMessageToConsole(typeMessage, message);
        }

        private async Task AddLogMessageToFile(TypeMessage typeMessage, LogMessage message)
        {
            try
            {
                //Esta metodo de repository usa logeo de Nlog , para logear en archivo
                await _repository.AddContent($"{typeMessage.ToString().ToUpper()}|{message.Description}");

                await _repository.AddContent(_configuracionesGenerales.FileLogPath, _configuracionesGenerales.FileLogName.Replace("{date}", DateTime.Now.ToString("dd-MM-yyyy")), $"{typeMessage.ToString().ToUpper()}|{message.Description}");
            }
            catch (Exception ex)
            {
                _logger.LogError($"No se ha podido agregar el mensaje en el archivo de log : {ex.Message}");
            }
        }

        private async Task AddLogMessageToDatabase(TypeMessage typeMessage, LogMessage message)
        {
            try
            {
                var logType = await _logTypeRepository.GetByName(typeMessage.ToString());

                await _uow.LogRepository.AddAsync(new Entities.Log(message.Description, logType.Id));
                await _uow.SaveAsync();
            }
            catch (Exception ex)
            {
                _logger.LogError($"No se ha podido agregar el log en la base de datos : {ex.Message}");
            }
        }

        private async Task AddLogMessageToConsole(TypeMessage typeMessage, LogMessage message)
        {
            try
            {
                await _service.AddLogLine(typeMessage, new ConsoleLogMessage { Description = message.Description });
            }
            catch (Exception ex)
            {
                _logger.LogError($"No se ha podido logear el log en la consola : {ex.Message}");
            }
        }
    }
}