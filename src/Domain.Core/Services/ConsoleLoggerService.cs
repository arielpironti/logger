﻿using Logger.Domain.Core.DTO.Request;
using Logger.Domain.Core.ExternalServices;
using System;
using System.Threading.Tasks;

namespace Logger.Domain.Core.Services
{
    public class ConsoleLoggerService : LoggerService<ConsoleLogMessage>, ILoggerService<ConsoleLogMessage>
    {
        private readonly IConsoleExternalService _service;

        public ConsoleLoggerService(IConsoleExternalService service)
            : base()
        {
            _service = service ?? throw new ArgumentNullException(nameof(service));
        }

        public async Task AddLogMessageAsync(TypeMessage typeMessage, ConsoleLogMessage message)
        {
            await AddMessageAsync(typeMessage, message);
        }

        protected override async Task AddMessageAsync(TypeMessage typeMessage, ConsoleLogMessage message)
        {
            await _service.AddLogLine(typeMessage, message);
        }
    }
}