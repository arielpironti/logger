﻿using Logger.Domain.Core.DTO.Request;
using System.Threading.Tasks;

namespace Logger.Domain.Core.Services
{
    public interface ILoggerService<TLogMessage>
        where TLogMessage : ILogMessage
    {
        Task AddLogMessageAsync(TypeMessage typeMessage, TLogMessage message);
    }

    public abstract class LoggerService<TLogMessage>
        where TLogMessage : ILogMessage
    {
        protected abstract Task AddMessageAsync(TypeMessage typeMessage, TLogMessage message);
    }
}