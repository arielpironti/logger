﻿using Logger.Domain.Core.Repositories;
using System.Threading.Tasks;

namespace Logger.Domain.Core.Data
{
    public interface IUnitOfWork
    {
        ILogRepository LogRepository { get; set; }

        Task SaveAsync();
    }
}