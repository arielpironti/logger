﻿using System.Threading.Tasks;

namespace Logger.Domain.Core.Repositories
{
    public interface IFileRepository
    {
        Task AddContent(string content);
        Task AddContent(string filePath, string fileName,string content);
    }
}