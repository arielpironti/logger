﻿using Logger.Domain.Entities;
using System.Threading.Tasks;

namespace Logger.Domain.Core.Repositories
{
    public interface ILogTypeRepository : IRepository<LogType, int>
    {
        Task<LogType> GetByName(string name);
    }
}