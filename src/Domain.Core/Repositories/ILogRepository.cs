﻿using Logger.Domain.Entities;

namespace Logger.Domain.Core.Repositories
{
    public interface ILogRepository : IRepository<Log, int>
    { }
}