﻿using Logger.Domain.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Logger.Domain.Core.Repositories
{
    public interface IRepository<T, Tkey> where T : BaseEntity<Tkey>
    {
        Task<IEnumerable<T>> GetAllAsync(bool tracking = false);

        Task AddAsync(T entity);
    }
}