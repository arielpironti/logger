﻿namespace Logger.Domain.Core.DTO.Config
{
    public class ConfiguracionesGeneralesConfig
    {
        public string FileLogName { get; set; }
        public string FileLogPath { get; set; }
    }
}