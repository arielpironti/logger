﻿using System.ComponentModel.DataAnnotations;

namespace Logger.Domain.Core.DTO.Request
{
    public class LogMessage:ILogMessage
    {
        [Required(AllowEmptyStrings =false)]
        public string Description { get; set; }
    }

}