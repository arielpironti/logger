﻿namespace Logger.Domain.Core.DTO.Request
{
    public interface ILogMessage
    {
        string Description { get; set; }
    }

    public enum TypeMessage
    {
        Message,
        Error,
        Warning,
        Information
    }
}