﻿using Logger.Domain.Core.DTO.Request;
using System.Threading.Tasks;

namespace Logger.Domain.Core.ExternalServices
{
    public interface IConsoleExternalService
    {
        Task AddLogLine(TypeMessage typeMessage, ConsoleLogMessage message);
    }
}