﻿using System;

namespace Logger.Domain.Core.Exceptions
{
    public class DuplicatedEntryException : Exception
    {
        public DuplicatedEntryException(string message)
            : base(message)
        { }
    }
}