﻿namespace Logger.Domain.Core.Exceptions
{
    public class EntityNotFoundException : UserException
    {
        public EntityNotFoundException(string value)
            : base(value) { }
    }
}