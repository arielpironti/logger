﻿using System;

namespace Logger.Domain.Core.Exceptions
{
    public class UserException : DomainException
    {
        public UserException(string errorMessage) : base(errorMessage)
        { }

        public UserException(string errorMessage, Exception innerException) : base(errorMessage, innerException)
        { }
    }
}