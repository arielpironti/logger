﻿using System;

namespace Logger.Domain.Core.Exceptions
{
    public class ExternalServiceException : DomainException
    {
        public ExternalServiceException(string errorMessage) : base(errorMessage)
        { }

        public ExternalServiceException(string errorMessage, Exception innerException) : base(errorMessage, innerException)
        { }
    }
}