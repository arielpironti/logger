﻿using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace Logger.Infrastructure.Helper
{
    public static class IQueryableExtensions
    {
        public static IQueryable<T> AsNoTracking<T>(this IQueryable<T> query, bool track) where T : class
            => track ? query : query.AsNoTracking();
    }
}