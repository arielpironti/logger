﻿using Logger.Domain.Core.Exceptions;
using Logger.Domain.Core.Repositories;
using Microsoft.Extensions.Logging;
using System;
using System.IO;
using System.Threading.Tasks;

namespace Logger.Infrastructure.Respositories.File
{
    public class FileLogRepository : IFileRepository
    {
        protected readonly ILogger<FileLogRepository> _logger;

        public FileLogRepository(ILogger<FileLogRepository> logger)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        //Usa directamente logeo de NLog en archivo
        public async Task AddContent(string content)
        {
            _logger.LogInformation(content);

            await Task.CompletedTask;
        }

        public async Task AddContent(string filePath, string fileName, string content)
        {
            try
            {
                using (StreamWriter writer = System.IO.File.AppendText($"{filePath}{fileName}"))
                {
                    writer.WriteLine(content);
                }

                await Task.CompletedTask;
            }
            catch (Exception ex)
            {
                throw new DomainException($"No fue posible estribir en archivo de log {filePath}{fileName}", ex);
            }
        }
    }
}