﻿using Logger.Domain.Core.Repositories;
using Logger.Domain.Entities;
using Logger.Infrastructure.Data.Ef;
using Logger.Infrastructure.Helper;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Logger.Infrastructure.Respositories.Database
{
    public abstract class EfRepository<T, TKey> : IRepository<T, TKey> where T : BaseEntity<TKey>
    {
        private readonly DataContext _db;
        protected DbSet<T> DbSet => _db.Set<T>();
        protected virtual IQueryable<T> BaseQuery { get { return DbSet.Select(x => x); } }

        protected EfRepository(DataContext db) => _db = db;

        protected virtual IQueryable<T> GetBaseQuery(bool tracking = false)
            => DbSet.Select(x => x).AsNoTracking(tracking);

        public virtual async Task AddAsync(T entity)
        {
            await DbSet.AddAsync(entity);
        }

        public virtual async Task<IEnumerable<T>> GetAllAsync(bool tracking = false)
            => await GetBaseQuery(tracking).ToListAsync();
    }
}