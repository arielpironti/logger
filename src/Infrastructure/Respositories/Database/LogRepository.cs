﻿using Logger.Domain.Core.Repositories;
using Logger.Domain.Entities;
using Logger.Infrastructure.Data.Ef;

namespace Logger.Infrastructure.Respositories.Database
{
    public class LogRepository : EfRepository<Log, int>, ILogRepository
    {
        public LogRepository(DataContext db) : base(db)
        { }
    }
}