﻿using Logger.Domain.Core.Exceptions;
using Logger.Domain.Core.Repositories;
using Logger.Domain.Entities;
using Logger.Infrastructure.Data.Ef;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace Logger.Infrastructure.Respositories.Database
{
    public class LogTypeRepository : EfRepository<LogType, int>, ILogTypeRepository
    {
        public LogTypeRepository(DataContext db) : base(db)
        { }

        public async Task<LogType> GetByName(string name)
        => await GetBaseQuery().FirstOrDefaultAsync(x => x.Name == name) ?? throw new EntityNotFoundException(name);
    }
}