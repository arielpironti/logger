﻿using Logger.Domain.Core.Exceptions;
using Logger.Domain.Core.Repositories;
using Logger.Domain.Entities;
using Logger.Infrastructure.Cache.Abstractions;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Logger.Infrastructure.Respositories.Database.Cache
{
    public class LogTypeRepositoryDecorator :
        BaseRepositoryDecorator<LogType, int, ILogTypeRepository>, ILogTypeRepository
    {
        public LogTypeRepositoryDecorator(ILogTypeRepository decorated, ICache cache)
            : base(decorated, cache) { }

        public async Task<LogType> GetByName(string name)
        {
            var coll = await _cache.GetOrDefaultAsync<IEnumerable<LogType>>(key) ?? Enumerable.Empty<LogType>();
            var resut = coll.FirstOrDefault(x => x.Name == name)
                ?? (await GetAllAsync(true)).FirstOrDefault(x => x.Name == name);

            if (resut == null)
                throw new EntityNotFoundException($"No se encontró un LogType para Codigo:{name}");

            return resut;
        }
    }
}