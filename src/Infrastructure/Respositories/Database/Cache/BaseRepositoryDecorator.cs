﻿using Logger.Domain.Core.Repositories;
using Logger.Domain.Entities;
using Logger.Infrastructure.Cache.Abstractions;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Logger.Infrastructure.Respositories.Database.Cache
{
    public abstract class BaseRepositoryDecorator<T, TKey, TRepo> : IRepository<T, TKey>
        where T : BaseEntity<TKey>
        where TRepo : IRepository<T, TKey>
    {
        protected readonly TRepo _decorated;
        protected readonly ICache _cache;
        protected readonly string key = typeof(T).Name;

        public BaseRepositoryDecorator(
            TRepo decorated,
            ICache cache)
        {
            if (decorated == null)
                throw new System.ArgumentNullException(nameof(decorated));

            _decorated = decorated;
            _cache = cache ?? throw new System.ArgumentNullException(nameof(cache));
        }

        public virtual async Task AddAsync(T entity)
        {
            await _cache.SetAsync(key, new List<T>());
            await _decorated.AddAsync(entity);
        }

        public virtual async Task<IEnumerable<T>> GetAllAsync(bool tracking = false)
        {
            //From Cache
            var itemsFromCache = await _cache.GetOrDefaultAsync<List<T>>(key);

            if (itemsFromCache != null && itemsFromCache.Any())
                return itemsFromCache;

            var decoratedColl = await _decorated.GetAllAsync(tracking);
            await _cache.SetAsync(key, decoratedColl);

            return decoratedColl;
        }
    }
}