﻿using Logger.Domain.Core.DTO.Request;
using Logger.Domain.Core.ExternalServices;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace Logger.Infrastructure.ExternalServices
{
    public class ConsoleExternalService : IConsoleExternalService
    {
        protected readonly ILogger<ConsoleExternalService> _logger;

        public ConsoleExternalService(ILogger<ConsoleExternalService> logger)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public async Task AddLogLine(TypeMessage typeMessage, ConsoleLogMessage message)
        {
            var msg = $"{typeMessage.ToString().ToUpper()}|{message.Description}";

            switch (typeMessage)
            {
                case TypeMessage.Message:
                    _logger.LogInformation(msg);
                    break;

                case TypeMessage.Warning:
                    _logger.LogWarning(msg);
                    break;

                case TypeMessage.Error:
                    _logger.LogError(msg);
                    break;

                default:
                    _logger.LogInformation(msg);
                    break;
            }

            await Task.CompletedTask;
        }
    }
}