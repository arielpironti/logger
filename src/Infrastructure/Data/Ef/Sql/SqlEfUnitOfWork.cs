﻿using Logger.Domain.Core.Exceptions;
using Logger.Domain.Core.Repositories;
using Microsoft.EntityFrameworkCore;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace Logger.Infrastructure.Data.Ef.Sql
{
    public class SqlEfUnitOfWork : EfUnitOfWork
    {
        protected sealed override DataContext Db { get; }

        public SqlEfUnitOfWork(
            DataContext db,
            ILogRepository logRepository)
            : base(logRepository) => Db = db ?? throw new System.ArgumentNullException(nameof(db));

        public override async Task SaveAsync()
        {
            try
            {
                await base.SaveAsync();
            }
            catch (DbUpdateException ex)
            {
                if (ex.InnerException?.InnerException is SqlException innerException && (innerException.Number == 2627 || innerException.Number == 2601))
                    throw new DuplicatedEntryException($"{innerException.Message}");

                throw;
            }
        }
    }
}