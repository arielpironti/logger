﻿using Logger.Domain.Core.Data;
using Logger.Domain.Core.Repositories;
using System.Threading.Tasks;

namespace Logger.Infrastructure.Data.Ef
{
    public abstract class EfUnitOfWork : IUnitOfWork
    {
        protected abstract DataContext Db { get; }

        public ILogRepository LogRepository { get; set; }

        protected EfUnitOfWork(ILogRepository logRepository)
        {
            LogRepository = logRepository;
        }

        public virtual async Task SaveAsync()
        => await Db.SaveChangesAsync();
    }
}