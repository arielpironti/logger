﻿using System;

namespace Logger.Domain.Entities
{
    public class Log : BaseEntity<int>
    {
        public string Description { get; private set; }

        public int LogTypeId { get; set; }
        public LogType LogType { get; private set; }

        public Log()
        { }

        public Log(string description, int logTypeId)
        {
            Description = description;
            LogTypeId = logTypeId;
            CreadoEn = DateTime.UtcNow;
        }
    }
}