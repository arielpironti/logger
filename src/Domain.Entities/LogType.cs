﻿namespace Logger.Domain.Entities
{
    public class LogType : BaseEntity<int>
    {
        public string Name { get; private set; }
    }
}