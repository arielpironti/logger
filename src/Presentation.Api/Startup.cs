using Autofac;
using Logger.Domain.Core.Data;
using Logger.Domain.Core.DTO.Config;
using Logger.Domain.Core.ExternalServices;
using Logger.Domain.Core.Repositories;
using Logger.Domain.Core.Services;
using Logger.Infrastructure.Cache.Abstractions;
using Logger.Infrastructure.Cache.Redis.DTO;
using Logger.Infrastructure.Data.Ef;
using Logger.Infrastructure.Data.Ef.Sql;
using Logger.Infrastructure.ExternalServices;
using Logger.Infrastructure.Respositories.Database;
using Logger.Infrastructure.Respositories.Database.Cache;
using Logger.Infrastructure.Respositories.File;
using Logger.Presentation.Api.Extensions;
using Logger.Presentation.Api.Middlewares;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Versioning;
using Microsoft.AspNetCore.Rewrite;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyModel;
using Swashbuckle.AspNetCore.Swagger;
using System;
using System.IO;
using System.Linq;
using System.Reflection;

namespace Logger.Presentation.Api
{
    public class Startup
    {
        public IConfiguration Configuration { get; private set; }

        public Startup(IHostingEnvironment env)
        {
            NLog.Web.NLogBuilder.ConfigureNLog(File.Exists($"nlog.{env.EnvironmentName}.config") ? $"nlog.{env.EnvironmentName}.config" : "nlog.config");

            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddEnvironmentVariables();

            Configuration = builder.BuildAndReplacePlaceholders();
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services
                .AddDbContextPool<DataContext>(options => options
                    .UseSqlServer(Configuration.GetConnectionString("DataContext")))
                .Configure<ConfiguracionesGeneralesConfig>(Configuration.GetSection("ConfiguracionesGenerales"))
                .Configure<RedisCacheOptions>(Configuration.GetSection("Redis"))
                .AddRedisCache()
                .AddApiVersioning(options =>
                 {
                     options.ApiVersionReader = new UrlSegmentApiVersionReader();
                     options.ReportApiVersions = true;
                     options.AssumeDefaultVersionWhenUnspecified = true;
                     options.DefaultApiVersion = new ApiVersion(1, 0);
                 })
                .AddSwaggerGen(c =>
                {
                    c.CustomSchemaIds(x => x.FullName);
                    c.SwaggerDoc("v1", new Info { Title = "Logger API", Version = "v1" });
                    c.DescribeAllEnumsAsStrings();
                    c.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, "Logger.Presentation.Api.xml"));
                })
                .AddMvc(options =>
                {
                    options.UseCentralRoutePrefix(new RouteAttribute("v{version:apiVersion}"));
                    options.Filters.Add(new ProducesAttribute("application/json"));
                })
               .SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
        }

        public void ConfigureContainer(ContainerBuilder builder)
        {
            var assemblies =
                DependencyContext
                    .Default
                    .GetDefaultAssemblyNames()
                    .Where(e => e.FullName.Contains("Logger"))
                    .Select(e => Assembly.Load(e))
                    .ToArray();

            builder
                .RegisterAssemblyTypes(assemblies)
                .AsClosedTypesOf(typeof(ILoggerService<>))
                .AsImplementedInterfaces();

            //External Services
            builder.RegisterType<ConsoleExternalService>().As<IConsoleExternalService>();

            // Npgsql Ef core unit of work and repositories
            builder.RegisterType<SqlEfUnitOfWork>().As<IUnitOfWork>();
            builder.RegisterType<LogRepository>().As<ILogRepository>();
            builder.RegisterType<FileLogRepository>().As<IFileRepository>();

            builder.RegisterType<LogTypeRepository>().Named<ILogTypeRepository>(nameof(LogTypeRepository));

            builder.RegisterDecorator<ILogTypeRepository>(
               (c, inner) => new LogTypeRepositoryDecorator(inner, c.Resolve<ICache>()),
               fromKey: nameof(LogTypeRepository));
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app
                .UseRewriter(new RewriteOptions().AddRedirect("^$", "/help"))
                .UseErrorHandling()
                .UseMvcWithDefaultRoute()
                .UseSwagger(c =>
                {
                    c.RouteTemplate = "help/docs/{documentName}/swagger.json";
                    c.PreSerializeFilters.Add((swagger, request) =>
                    {
                        if (request.Headers.ContainsKey("x-base-path"))
                            swagger.BasePath = $"/{request.Headers["x-base-path"].ToString().Trim('/')}";
                    });
                })
                .UseSwaggerUI(c =>
                {
                    c.SwaggerEndpoint("docs/v1/swagger.json", "Logger Api");
                    c.RoutePrefix = "help";
                    c.DefaultModelsExpandDepth(-1);
                });
        }
    }
}