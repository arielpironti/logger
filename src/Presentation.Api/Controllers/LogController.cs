﻿using Logger.Domain.Core.DTO.Request;
using Logger.Domain.Core.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace Logger.Presentation.Api.Controllers
{
    [Route("log")]
    public class LogController : Controller
    {
        private readonly ILoggerService<FileLogMessage> _fileLogService;
        private readonly ILoggerService<ConsoleLogMessage> _consoleLogService;
        private readonly ILoggerService<DatabaseLogMessage> _databaseLogService;
        private readonly ILoggerService<LogMessage> _logService;

        private static string _test;

        public LogController(ILoggerService<FileLogMessage> fileLogService,
                             ILoggerService<ConsoleLogMessage> consoleLogService,
                             ILoggerService<DatabaseLogMessage> databaseLogService,
                             ILoggerService<LogMessage> logService)
        {
            _fileLogService = fileLogService ?? throw new ArgumentNullException(nameof(fileLogService));
            _consoleLogService = consoleLogService ?? throw new ArgumentNullException(nameof(consoleLogService));
            _databaseLogService = databaseLogService ?? throw new ArgumentNullException(nameof(databaseLogService));
            _logService = logService ?? throw new ArgumentNullException(nameof(logService));
            _test = "123";
        }

        /// <summary>
        /// Permite logear un mensaje en base de datos, consola y archivo
        /// </summary>
        [HttpPost]
        [Route("{type}")]
        public async Task LogMessage([FromRoute] TypeMessage type,
                                     [FromBody] LogMessage message)
        {
            await _logService.AddLogMessageAsync(type, message);
        }

        /// <summary>
        /// Permite logear un mensaje en archivo.
        /// </summary>
        [HttpPost]
        [Route("{type}/file")]
        public async Task FileLogMessage([FromRoute] TypeMessage type,
                                         [FromBody] FileLogMessage message)
        {
            await _fileLogService.AddLogMessageAsync(type, message);
        }

        /// <summary>
        /// Permite logear un mensaje en consola.
        /// </summary>
        [HttpPost]
        [Route("{type}/console")]
        public async Task ConsoleLogMessage([FromRoute] TypeMessage type,
                                         [FromBody] ConsoleLogMessage message)
        {
            await _consoleLogService.AddLogMessageAsync(type, message);
        }

        /// <summary>
        /// Permite logear un mensaje en base de datos.
        /// </summary>
        [HttpPost]
        [Route("{type}/database")]
        public async Task DatabseLogMessage([FromRoute] TypeMessage type,
                                         [FromBody] DatabaseLogMessage message)
        {
            await _databaseLogService.AddLogMessageAsync(type, message);
        }
    }
}