﻿using Logger.Domain.Core.Exceptions;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Net;
using System.Threading.Tasks;

namespace Logger.Presentation.Api.Middlewares
{
    public static class ErrorHandlingMiddlewareExtensions
    {
        public static IApplicationBuilder UseErrorHandling(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<ErrorHandlingMiddleware>();
        }
    }

    public class ErrorHandlingMiddleware
    {
        private readonly RequestDelegate _next;

        private readonly IHostingEnvironment _env;

        public ErrorHandlingMiddleware(RequestDelegate next,
            IHostingEnvironment env)
        {
            _next = next;
            _env = env;
        }

        public async Task Invoke(HttpContext context)
        {
            try
            {
                await _next(context);
            }
            catch (Exception ex)
            {
                await HandleExceptionAsync(context, ex);
            }
        }

        private Task HandleExceptionAsync(HttpContext context, Exception exception)
        {
            var code = HttpStatusCode.InternalServerError;
            string message = exception.Message;

            if (exception is UnauthorizedAccessException)
            {
                code = HttpStatusCode.Forbidden;
            }
            else if (exception is EntityNotFoundException)
            {
                code = HttpStatusCode.NotFound;
            }
            else if (exception is UserException)
            {
                code = HttpStatusCode.BadRequest;
            }
            else if (exception is DuplicatedEntryException)
            {
                code = HttpStatusCode.Conflict;
            }
            else if (exception is ExternalServiceException)
            {
                code = HttpStatusCode.BadGateway;
            }
            else if (exception is DomainException)
            {
                code = HttpStatusCode.InternalServerError;
            }
            else
            {
                message = "Internal server error";
            }

            var result = JsonConvert.SerializeObject(new { error = message });

            context.Response.ContentType = "application/json";
            context.Response.StatusCode = (int)code;

            return context.Response.WriteAsync(result);
        }

        private string BuildExceptionMessage(Exception exc)
        {
            string message = string.Empty;

            try { message += $"\r\n\t{exc.GetType().ToString()}: {exc.Message}"; }
            catch { }
            if (exc.InnerException != null)
                message += BuildExceptionMessage(exc.InnerException);

            return message;
        }

        private string BuildMessage(Exception exc, HttpStatusCode code, string referer = null)
        {
            string message = string.Empty;

            message += BuildExceptionMessage(exc);
            if (string.IsNullOrEmpty(referer) == false)
                try { message += $"\r\n\tReferer: {referer}"; }
                catch { }
            try { message += $"\r\n\tResponse: Status {(int)code} ({code.ToString()})"; }
            catch { }

            return message;
        }
    }
}