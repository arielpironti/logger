﻿using Logger.Infrastructure.Cache.Abstractions;
using Logger.Infrastructure.Cache.Redis;
using Logger.Infrastructure.Cache.Redis.DTO;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using StackExchange.Redis;
using System;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddRedisCache(this IServiceCollection services)
        {
            if (services == null)
                throw new ArgumentNullException(nameof(services));

            services.AddSingleton<ICacheTimeProvider, CacheTimeProvider>();
            services.AddSingleton(typeof(IConnection<IDatabase>), sp =>
                new RedisConnection(sp.GetRequiredService<IOptions<RedisCacheOptions>>().Value.ConnectionString, sp.GetRequiredService<ILogger<RedisConnection>>()));
            services.AddScoped<ICache>(sp => new RedisCache(
                sp.GetRequiredService<IConnection<IDatabase>>(),
                sp.GetRequiredService<ICacheTimeProvider>(),
                sp.GetRequiredService<IOptions<RedisCacheOptions>>()
            ));
            return services;
        }
    }
}