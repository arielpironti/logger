using Logger.Infrastructure.Cache.Abstractions;
using Logger.Infrastructure.Cache.Abstractions.Exceptions;
using Logger.Infrastructure.Cache.Redis.DTO;
using Microsoft.Extensions.Options;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Logger.Infrastructure.Cache.Redis
{
    public class RedisCache : ICache
    {
        // KEYS[1] = = key
        // ARGV[1] = absolute-expiration - ticks as long (-1 for none)
        // ARGV[2] = data - byte[]
        private const string SetScript = ("return redis.call('HMSET', KEYS[1], 'absexp', ARGV[1], 'data', ARGV[2])");

        private const string GetScript = ("return redis.call('HMGET', KEYS[1], 'data')");

        private const string ClearCacheScript = @"
            for _,k in ipairs(redis.call('KEYS', ARGV[1])) do
                redis.call('DEL', k)
            end";

        private const string GetKeysScript = "return redis.call('KEYS', ARGV[1])";

        // Default expiration (1 hour) if not set in method call nor configuration
        public const long DefaultExpirationInSeconds = 3600;

        private readonly ICacheTimeProvider _timeProvider;

        private readonly IConnection<IDatabase> _connection;

        private readonly string _instance;
        private readonly long _defaultExpirationInSeconds;

        private readonly SemaphoreSlim _connectionLock = new SemaphoreSlim(initialCount: 1, maxCount: 1);

        public RedisCache(
            IConnection<IDatabase> connection,
            ICacheTimeProvider timeProvider,
            IOptions<RedisCacheOptions> optionsAccessor)
            : this(connection, timeProvider, optionsAccessor?.Value.InstanceName, optionsAccessor?.Value.DefaultExpirationInSeconds)
        { }

        public RedisCache(
            IConnection<IDatabase> connection,
            ICacheTimeProvider timeProvider,
            string instanceName = null,
            long? defaultExpirationInSeconds = null)
        {
            _connection = connection ?? throw new ArgumentNullException(nameof(connection));
            _timeProvider = timeProvider ?? throw new ArgumentNullException(nameof(timeProvider));

            // This allows partitioning a single backend cache for use with multiple apps/services.
            _instance = instanceName ?? string.Empty;

            // Forces to have an expiration.
            _defaultExpirationInSeconds = defaultExpirationInSeconds ?? DefaultExpirationInSeconds;
        }

        public async Task SetAsync(string key, object value, long? expirationInSeconds = null)
        {
            if (string.IsNullOrEmpty(key))
                throw new ArgumentNullException(nameof(key));

            if (value == null)
                throw new ArgumentNullException(nameof(value));

            var _cache = await _connection.GetDatabaseOrDefaultAsync();
            if (_cache == null)
                return;

            expirationInSeconds = expirationInSeconds ?? _defaultExpirationInSeconds;
            var absoluteExpiration = _timeProvider.GetAbsoluteDateTime(expirationInSeconds.Value);

            await _cache.ScriptEvaluateAsync(SetScript,
                new RedisKey[] { _instance + key },
                new RedisValue[] { absoluteExpiration.Ticks, value.ToByteArray() });
        }

        public virtual async Task<T> GetAsync<T>(string key)
        {
            var bytes = await GetBytesAsync(key);

            if (bytes == null)
                throw new NotFoundInCacheException(key);

            return bytes.ToObject<T>();
        }

        public virtual async Task<T> GetOrDefaultAsync<T>(string key)
        {
            try
            {
                return await GetAsync<T>(key);
            }
            catch (NotFoundInCacheException)
            {
                return default(T);
            }
        }

        public virtual async Task<bool> IsInCacheAsync(string key)
        {
            try
            {
                return await GetBytesAsync(key) != null;
            }
            catch (NotFoundInCacheException)
            {
                return false;
            }
        }

        public async Task<List<string>> GetAllKeysAsync(string pattern = null)
        {
            var _cache = await _connection.GetDatabaseOrDefaultAsync();
            if (_cache == null)
                return new List<string>();

            var filter = string.IsNullOrEmpty(pattern) ? $"{_instance}*" : $"{_instance}*{pattern}*";

            var result = await _cache.ScriptEvaluateAsync(
                GetKeysScript,
                values: new RedisValue[] { filter });

            if (result == null || result.IsNull)
                return new List<string>();

            return ((RedisResult[])result)?
                .Select(x => x.ToString())
                .Where(x => x.Contains(_instance))
                .Select(x => x.Substring(_instance.Length))
                .ToList();
        }

        private async Task<byte[]> GetBytesAsync(string key)
        {
            if (key == null)
                throw new ArgumentNullException(nameof(key));

            var _cache = await _connection.GetDatabaseOrDefaultAsync();
            if (_cache == null)
                return null;

            var result = await _cache.ScriptEvaluateAsync(GetScript, new RedisKey[] { _instance + key });
            
            if (result == null || result.IsNull)
                return null;

            var resultValues = (RedisValue[])result;
            return resultValues.Any() ? resultValues[0] : RedisValue.Null;
        }

        public async Task ClearAsync() => await RemoveAllAsync("*");

        public async Task RemoveAllAsync(string pattern)
        {
            if (string.IsNullOrEmpty(pattern))
                throw new ArgumentNullException(nameof(pattern));

            var _cache = await _connection.GetDatabaseOrDefaultAsync();
            if (_cache == null)
                return;
            await _cache.ScriptEvaluateAsync(ClearCacheScript, values: new RedisValue[] { _instance + pattern });
        }

        public async Task RemoveAsync(params string[] keys)
        {
            if (keys == null || keys.Any(k => string.IsNullOrEmpty(k)))
                throw new ArgumentNullException(nameof(keys));

            var _cache = await _connection.GetDatabaseOrDefaultAsync();
            if (_cache == null)
                return;

            var keysArray = keys.Select(x => (RedisKey)(_instance + x)).ToArray();

            await _cache.KeyDeleteAsync(keysArray);
        }
    }
}