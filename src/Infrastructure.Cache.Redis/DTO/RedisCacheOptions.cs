﻿namespace Logger.Infrastructure.Cache.Redis.DTO
{
    public class RedisCacheOptions
    {
        public string ConnectionString { get; set; }

        public string InstanceName { get; set; }

        public long? DefaultExpirationInSeconds { get; set; }

        public RedisCacheOptions()
        {}

        public RedisCacheOptions(string connectionString, string instanceName, long? defaultExpirationInSeconds = null)
        {
            ConnectionString = connectionString ?? throw new System.ArgumentNullException(nameof(connectionString));
            InstanceName = instanceName ?? throw new System.ArgumentNullException(nameof(instanceName));
            DefaultExpirationInSeconds = defaultExpirationInSeconds;
        }
    }
}