using Logger.Infrastructure.Cache.Abstractions;
using Microsoft.Extensions.Logging;
using StackExchange.Redis;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Logger.Infrastructure.Cache.Redis
{
    public class RedisConnection : IConnection<IDatabase>
    {
        private readonly string _configuration;
        private readonly ILogger<RedisConnection> _logger;
        private volatile ConnectionMultiplexer _connection;
        private IDatabase _cache;

        private readonly SemaphoreSlim _connectionLock = new SemaphoreSlim(initialCount: 1, maxCount: 1);

        public bool IsConnected => _connection?.IsConnected ?? false;

        public RedisConnection(string connString, ILogger<RedisConnection> logger)
        {
            _configuration = connString ?? throw new ArgumentNullException(nameof(connString));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public async Task<bool> TryConnectAsync()
        {
            try
            {
                await ConnectAsync();
                return IsConnected && _cache != null;
            }
            catch
            {
                return false;
            }
        }

        public async Task<IDatabase> GetDatabaseAsync()
        {
            if (await TryConnectAsync() == false)
                throw new InvalidOperationException();

            return _cache;
        }

        public async Task<IDatabase> GetDatabaseOrDefaultAsync()
        {
            try
            {
                return await GetDatabaseAsync();
            }
            catch
            {
                return null;
            }
        }

        public async Task ConnectAsync()
        {
            if (_cache != null && IsConnected)
                return;

            await _connectionLock.WaitAsync();
            try
            {
                if (_cache == null || !IsConnected)
                {
                    _connection = await ConnectionMultiplexer.ConnectAsync(_configuration);
                    _cache = _connection.GetDatabase();
                    _logger.LogInformation("Redis: Connection established");
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Redis: Connection could not be established");
                throw;
            }
            finally
            {
                _connectionLock.Release();
            }
        }

        public void Dispose()
        {
            try
            {
                if (_connection != null)
                    _connection.Close();
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, "Redis: Connection could not be disposed");
            }
        }
    }
}