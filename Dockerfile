FROM microsoft/dotnet:2.1-sdk AS build
WORKDIR /app

COPY NuGet.config ./
COPY *.sln .
COPY src/Domain.Core/Domain.Core.csproj ./src/Domain.Core/
COPY src/Domain.Entities/Domain.Entities.csproj ./src/Domain.Entities/
COPY src/Infrastructure/Infrastructure.csproj ./src/Infrastructure/
COPY src/Infrastructure.Cache.Abstractions/Infrastructure.Cache.Abstractions.csproj ./src/Infrastructure.Cache.Abstractions/
COPY src/Infrastructure.Cache.Redis/Infrastructure.Cache.Redis.csproj ./src/Infrastructure.Cache.Redis/
COPY src/Presentation.Api/Presentation.Api.csproj ./src/Presentation.Api/
COPY test/Domain.Core.Test/Domain.Core.Test.csproj ./test/Domain.Core.Test/


RUN dotnet restore

COPY . ./

RUN dotnet publish /app/src/Presentation.Api -o out

FROM microsoft/dotnet:2.1.2-aspnetcore-runtime-alpine AS runtime
WORKDIR /app
COPY --from=build /app/src/Presentation.Api/out ./
ENTRYPOINT ["dotnet", "Logger.Presentation.Api.dll"]