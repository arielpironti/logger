**Logger Service**

-**API** que provee funciolidad para logear en diferentes recursos, como ser File,Console,Database.

-**Observaciones**: 
- En caso de ejecutar el servicio por primera vez , se debera ejecutar el script , que se encuentra en la carpeta **scripts**.
- Se debera configurar las variables de entorno de los archivos **launchSettings.json** y/o **docker-composer.yml** , según corresponda donde se ejecute la aplicación.


##Métodos que provee la API

**POST http:/host../log/{type}** (Permite logear un mensaje en base de datos, consola y archivo.)


**POST http:/host../log/{type}/file** (Permite logear un mensaje en archivo.)


**POST http:/host../{type}/console** (Permite logear un mensaje en consola.)


**POST http:/host../{type}/databse** (Permite logear un mensaje en base de datos.)


**Obs:** 
- Los tipós de logs disponibles son **Message,Warning y Error**
- Se requiere enviar la sigueinte estructura con la descripcion del log:
 ```json
 {
   "description":"Descripcion del log"
 }
 ```
